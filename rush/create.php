<?php
if (!$_SESSION)
	session_start();

if ($_POST['submit'] == "OK" && $_POST['submit'] && $_POST['login'] && $_POST['passwd']) {
	if (!file_exists('./private/passwd'))
		mkdir("./private");
	$user = unserialize(file_get_contents('./private/passwd'));
	foreach ($user as $key => $value) {
		if ($value['login'] == $_POST['login'])
		{
			header('Location: http://localhost:8100/rush/reg_exist.html');
			exit("ERROR\n");
		}
	}
	$user[] = array("login" => $_POST['login'], "passwd" => hash("sha256", $_POST['passwd']));
	file_put_contents('./private/passwd', serialize($user));
	$_SESSION["id"] = $_POST["login"];
	header('Location: http://localhost:8100/rush/index.php');
	echo "OK\n";
}
else
{
	header('Location: http://localhost:8100/rush/reg_error.html');
	echo "ERROR\n";
}
?>
