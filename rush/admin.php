<?php  if($_POST["user"])
  {
        if (!file_exists('./private/passwd'))
          exit("file not found");
        $users = unserialize(file_get_contents('./private/passwd'));
        foreach ($users as $key => $value)
        {
          if ($value['login'] == $_POST["user"])
          {
               unset($users[$key]);
               array_filter($users);
               file_put_contents('./private/passwd', serialize($users));

               header('Location: http://localhost:8100/rush/admin.php');
               break ;
          }
        }

  }
  elseif($_POST["article"])
  {
        if (!file_exists('.data'))
          exit("file not found");
        $users = unserialize(file_get_contents('.data'));
        foreach ($users as $key => $value)
        {
          if ($value['category'] == $_POST["article"])
          {
               unset($users[$key]);
               array_filter($users);
               file_put_contents('.data', serialize($users));

               header('Location: http://localhost:8100/rush/admin.php');
               break ;
          }
        }

  }
  ?>
  <!DOCTYPE html>
<html lang="en">
 <head>
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta charset="utf-8">
   <title>Safari Tour</title>
   <link rel="stylesheet" href="css/main.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 </head>
 <body class="body body-flexbox">
   <header class="header header-flexbox">
     <div class="logo logo-flexbox">
       <h2 class="logo__titel">Safari Tour </h2>
       <h3 class="logo__titel_text">International Tour packages</h3>
     </div>
     <nav class="nav">
       <ul class="nav__list nav-flexbox">
         <li class="nav__item"><a class="nav__item__links" href="http://localhost:8100/rush/index.php">Home</a></li>
       </li>
       </ul>
     </nav>
   </header>
   <?php
   session_start();
   if ($_SESSION['validuser'] !== 'admin' && $_SESSION['validuser'] !== 'error') {?>
   <form calss="form" action="login_admin.php" method="POST">
         <ul class="footer__contact__form__list">
             <li class="footer__contact__item">
                 <input class="input footer__contact__item__input-size" input type="text" name="admin_login" value=""  placeholder="login">
             </li>
             <li class="footer__contact__item">
                 <input class="input footer__contact__item__input-size" input type="password" name="admin_passwd" value="" placeholder="password">
           </li>
             <li class="footer__contact__item">
                 <input class="submit footer__contact__button-right" type="submit" name="admin_enter" value="OK">
           </li>
         </ul>
     </form>
   <?php } else if ($_SESSION['validuser'] == 'admin'){ ?>
    <form  method="post" action="add_prod.php" method="POST">
           <p class="">Name</p>
           <input class="" type="text" name="name" value=""/>
           <br>
           <p class="">Category</p>
           <input class="" type="text" name="category" value=""/>
           <p class="">Price</p>
           <input class="" type="text" name="price" value=""/>
           <p class="">Image(url or local)</p>
           <input class="" type="text" name="img" value=""/>
           <br>
           <input  type="submit" name="submit" value="submit"/>
           <br>
      </form>
      <?php
      $users = unserialize(file_get_contents('./private/passwd'));
      foreach ($users as $key => $value) {
        ?> <li><span><?php echo $value['login']; ?> </span></li><?php
      }
      $data = unserialize(file_get_contents('.data'));
        foreach ($data as $key => $value) {
        ?> <li><span><?php echo $value['category']; ?> </span><span><?php echo $value['name']; ?> </span></li><?php
      }
      ?>
        <form  method="post" action="admin.php" method="POST">
          <p class="">User to delete</p>
          <input class="" type="text" name="user" value=""/>
           <br>
           <input  type="submit" name="submit" value="submit"/>
           <br>
        </form>
        <form  method="post" action="admin.php" method="POST">
          <p class="">remove article</p>
          <input class="" type="text" name="article" value=""/>
           <br>
           <input  type="submit" name="submit" value="submit"/>
           <br>
        </form>
        <?php } else {?>
      <section class="reg_exist sectionOne sectionOne-flexbox-row sectionOne-flexbox-mobile">
        <h2 class="logo__titel">Incorrect login or password, try again</h2>
      </section>
    <?php } ?>
 </body>
</html>
</html>
